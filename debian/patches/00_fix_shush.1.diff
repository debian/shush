#
# Description: Fixes for shush.1 manpage.
#
#  Deals with a few lintian warnings and info messages:
#
#   I: shush: hyphen-used-as-minus-sign usr/share/man/man1/shush.1.gz:32
#   I: shush: hyphen-used-as-minus-sign usr/share/man/man1/shush.1.gz:236
#   I: shush: hyphen-used-as-minus-sign usr/share/man/man1/shush.1.gz:307
#   I: shush: hyphen-used-as-minus-sign usr/share/man/man1/shush.1.gz:399
#   I: shush: hyphen-used-as-minus-sign usr/share/man/man1/shush.1.gz:404
#   I: shush: hyphen-used-as-minus-sign usr/share/man/man1/shush.1.gz:436
#   I: shush: spelling-error-in-manpage usr/share/man/man1/shush.1.gz allows to allows one to
#   W: shush: manpage-has-errors-from-man usr/share/man/man1/shush.1.gz 2: warning: macro `DA' not defined
#   W: shush: manpage-has-errors-from-man usr/share/man/man1/shush.1.gz 278: expected `;' after scale-indicator (got `r')
#   W: shush: manpage-has-errors-from-man usr/share/man/man1/shush.1.gz 337: warning: numeric expression expected (got `o')
#
# Author: Dario Minnucci <midget@debian.org>
# Forwarded: Christophe Kalt <shush@taranis.org>
# Updated: 2013-12-30
#
Index: shush/shush.1
===================================================================
--- shush.orig/shush.1	2013-12-30 00:14:03.582414440 +0100
+++ shush/shush.1	2013-12-30 00:15:22.313236611 +0100
@@ -1,5 +1,4 @@
 .TH SHUSH 1 "$Date: 2007-09-30 23:38:23 $"
-.DA December 29, 2006
 .SH NAME
 shush - Run a command and optionally report its output by mail
 .SH SYNOPSIS
@@ -30,7 +29,7 @@
 .B -R \fIto\fP
 ] [
 .B -T \fIto\fP
-] -C \fIname\fP [ \fIstdout\fP [ \fIstderr\fP ] ]
+] \-C \fIname\fP [ \fIstdout\fP [ \fIstderr\fP ] ]
 
 .B shush
 [
@@ -234,7 +233,7 @@
 This parameter allows one to control how long the \fIcommand\fP may run.
 It should be a comma separated list of actions.  Valid actions are: a time
 duration (during which \fBshush\fP should simply wait for the \fIcommand\fP
-to terminate), a signal (either "SIGNAME" or "-SIGNUMBER") that should be
+to terminate), a signal (either "SIGNAME" or "\-SIGNUMBER") that should be
 sent to the \fIcommand\fP's process group, a signal (either "=SIGNAME" or
 "=SIGNUMBER") that should be sent to the \fIshell\fP used to spawn the
 \fIcommand\fP, the string "loop" (to mark where to start again from when
@@ -272,11 +271,13 @@
 "\fBsubject\fP" contains any "%" character.
 .IP "\fBuserprefix\fP"
 By default, specified subjects are prefixed with the username between
-brackets.  This parameter allows to disable this prefix.  Any non zero value
+brackets.  This parameter allows one to disable this prefix.  Any non zero value
 indicates that the username should be shown while zero causes the prefix to
 be omitted.  This parameter is ignored if the "\fBsubject\fP" contains any "%"
 character.
-.IP "\fBoutput\fP" (previously "\fBstderr\fB")
+.IP "\fBoutput\fP"
+(previously "\fBstderr\fB")
+
 This defines how the command's standard output and standard error are
 captured and reported to the user:  "errfirst", "mixed", "outfirst".  When
 using "mixed", the \fIname.stderr\fP configuration file is ignored.  When
@@ -303,7 +304,7 @@
 .IP "\fB$exit\fP"
 If the command terminated normally, this is its exit code.  Otherwise, it
 is negative and indicates the signal number having caused the command to
-terminate (e.g. -1 indicates signal number 1 caused the command to terminate).
+terminate (e.g. \-1 indicates signal number 1 caused the command to terminate).
 .IP "\fB$size\fP"
 output size (in bytes), same as "$outsize + $errsize"
 .IP "\fB$outsize\fP"
@@ -333,7 +334,10 @@
 The "%" character
 .IP "\fB%h\fP"
 The hostname
-.IP "\fB%<digit>\fP" or "\fB%-<digit>\FP"
+.IP "\fB%<digit>\fP"
+or
+"\fB%-<digit>\FP"
+
 A partial hostname: A positive digit indicates how many components of the
 fully qualified hostname to keep; a negative digit indicates how many
 trailing components of the fully qualified hostname to trim.
@@ -392,12 +396,12 @@
 Directory where temporary files are created.
 
 .SH EXAMPLE
-The following configuration runs "shush -c /etc/shush -u" daily at 9:00,
+The following configuration runs "shush \-c /etc/shush \-u" daily at 9:00,
 updating the user (root) crontab:
 
 .RS
 .nf
-command=shush -c /etc/shush -u
+command=shush \-c /etc/shush \-u
 schedule=0 9 * * *
 lock=notify=root root-logs,abort
 timeout=5m,loop,notify=root root-logs,15m
@@ -429,7 +433,7 @@
 A lock will be set while running the command, and mail sent to "root" and
 "root-logs" if the lock is held by another process when \fBshush\fP starts,
 in which case \fBshush\fP will abort.  A mail will also be sent to "root"
-and "root-logs" if "shush -c /etc/shush -u" runs for more than 5 minutes,
+and "root-logs" if "shush \-c /etc/shush \-u" runs for more than 5 minutes,
 and for every 15 minutes following the first 5 minutes.
 
 Upon completion, the output will always be sent to "root-logs".
